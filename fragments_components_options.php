<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}




/**
 * globals array identite_extra
 *
 * Ajout des champs generiques pour identite extra
 * ce tableau pouvant etre surcharge depuis le plugin squelette ou theme
 * depuis le fichier prefix_options.php
 *
*/
if(test_plugin_actif('identite_extra')) {
	$GLOBALS['identite_extra'] = array(
		'adresse',
    	'ville',
      'code_postal',
      'region',
      'pays',
      'telephone',
		//
		'raison_sociale',
		'regime_fiscal',
		'code_ape',
    'siret',
    'tva_intra',
		'google_maps_coords', // modele/identite_extra_maps
		// Mentions Legales
		"webmaster_nom",
		"responsable_publi",
		"proprio_nom",
		"proprio_prenom",
		"proprio_email",
		"hebergeur_nom",
		"hebergeur_adresse",
		"hebergeur_telephone"
    );
}

define('_EMOGRIFIER_CSS','emails/emails.css,newsletters/styles.css');
