# CHANGELOG

1.0.8

menu/minipanier : si commande en cours on affiche le lien vers la page commande plutot que panier vide

doc readme

1.0.6

migration svn vers git

1.0.3
Sun Dec 24 18:46:54 2017

*	correction sur le formulaire_contact, on utilise facteur

1.0.2
Sun Jun 05 10:00:24 2016 :

*  suppression du menu panier redondant avec minipanier
* ajout d'un parm url page sur le menu mini panier pour pouvoir l'utiliser dans tout types de parcours paiements
