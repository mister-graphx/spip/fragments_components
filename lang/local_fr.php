<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/dist/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
  'form_newsletter_subscribe_explication'=>'En vous inscrivant, vous acceptez de recevoir (via @nom_site_spip@) des informations et actualité .

   Votre droit de rectification et d’oubli peut être obtenu aisément en nous contactant via notre page de contact : Cliquez ici',

);
