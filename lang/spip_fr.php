<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/dist/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
  'form_forum_confirmer_email' => 'Pour confirmer votre adresse email, <a href="@url_confirm@">rendez-vous à cette adresse </a>',
  'form_forum_voici1' => 'Voici vos identifiants pour pouvoir participer à la vie du site <a href"@adresse_site@">@nom_site_spip@</a> :',
  'form_forum_voici2' => 'Voici vos identifiants pour proposer des articles sur
  le site "@nom_site_spip@" (<a href="@adresse_login@">@adresse_login@</a>) :',
  'form_ecrire_auteur_info_previsu'=>'Vous pouvez relire le texte de votre message avant de le soumettre',
  'form_newsletter_subscribe_explication'=>'En vous inscrivant, vous acceptez de recevoir (via @nom_site_spip@) des informations et actualités.

   Votre droit de rectification et d’oubli peut être obtenu aisément en <a href="@url_contact@">nous contactant</a>.

   Vous pouvez vous désinscrire à tout moment en <a href="@url_unsubscribe@">suivant ce lien</a>',


);
