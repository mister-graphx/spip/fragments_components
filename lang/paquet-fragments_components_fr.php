<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

    // F
	'fragments_components_description' => 'Collection de composant spip',
	'fragments_components_slogan' => 'Biblioth&egrave;que de composants spip r&eacute;utilisables',
);
?>