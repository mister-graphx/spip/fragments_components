<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * chargement des valeurs par defaut des champs du #FORMULAIRE_NAV_RECHERCHE
 *
 * @param css
 * @param placeholder
 * @param titre
 * @param masquer_label
 * @return array
 */
function formulaires_nav_recherche_charger_dist($titre ='', $css = '', $placeholder='', $masquer_label= '') {
	if ($GLOBALS['spip_lang'] != $GLOBALS['meta']['langue_site']) {
		$lang = $GLOBALS['spip_lang'];
	} else {
		$lang = '';
	}

	$action = generer_url_public('recherche'); # action specifique, ne passe pas par Verifier, ni Traiter
	return
		array(
			'action' => $action,
			'recherche' => _request('recherche'),
			'lang' => $lang,
			'titre' => $titre,
			'css' => $css,
			'placeholder' => $placeholder,
			'masquer_label'=> $masquer_label
		);
}
